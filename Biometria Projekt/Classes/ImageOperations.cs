﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using Biometria_Projekt.Classes;

namespace Biometria_Projekt
{
    class ImageOperations
    {

        public ImageProperties imgProperties { get; set; }

        public ImageOperations()
        {
            imgProperties = new ImageProperties();
        }

        public static double SearchMaxValue(byte[] array)
        {
            double max = 0;
            for (int i = 0; i < array.Length; i += 4)
            {
                if (array[i] > max) max = array[i];
                if (array[i + 1] > max) max = array[i + 1];
                if (array[i + 2] > max) max = array[i + 2];
            }
            return max;
        }

        public static double SearchMinValue(byte[] array)
        {
            double min = 255;
            for (int i = 0; i < array.Length; i += 4)
            {
                if (array[i] < min) min = array[i];
                if (array[i + 1] < min) min = array[i + 1];
                if (array[i + 2] < min) min = array[i + 2];
            }
            return min;
        }

        public static double SearchMaxValue2(byte[] array)
        {
            double max = 0;
            for (int i = 0; i < array.Length; i += 4)
            {
                int v = (array[i] + array[i + 1] + array[i + 2]) / 3;
                if (v > max) max = v;
            }
            return max;
        }

        public static double SearchMinValue2(byte[] array)
        {
            double min = 255;
            for (int i = 0; i < array.Length; i += 4)
            {
                int v = (array[i] + array[i + 1] + array[i + 2]) / 3;
                if (v < min) min = v;
            }
            return min;
        }

        public static int GetIndexOfPixel(int X, int Y, int _stride)
        {
            int x = 4 * X;
            int y = _stride * Y;
            return x + y;
        }

        public static int GetColorOfPixel(int X, int Y, int _stride, byte[] pixels)
        {
            var index = GetIndexOfPixel(X, Y, _stride);
            return pixels[index];
        }

        public static bool CheckIfOnlyWhite(int X, int Y, int _stride, byte[] pixels)
        {
            for (int i = X - 1; i <= X + 1; i++)
            {
                if (GetColorOfPixel(i, Y - 1, _stride, pixels) == 0)
                {
                    return false;
                }
            }

            if (GetColorOfPixel(X - 1, Y, _stride, pixels) == 0 || GetColorOfPixel(X - 1, Y, _stride, pixels) == 0)
            {
                return false;
            }

            for (int i = X - 1; i <= X + 1; i++)
            {
                if (GetColorOfPixel(i, Y + 1, _stride, pixels) == 0)
                {
                    return false;
                }
            }

            //var index1 = GetColorOfPixel(X - 1, Y - 1, _stride, pixels);
            //var index2 = GetColorOfPixel(X, Y - 1, _stride, pixels);
            //var index3 = GetColorOfPixel(X + 1, Y - 1, _stride, pixels);


            //var index4 = GetColorOfPixel(X - 1, Y, _stride, pixels);
            //var index5 = GetColorOfPixel(X + 1, Y, _stride, pixels);

            //var index6 = GetColorOfPixel(X - 1, Y + 1, _stride, pixels);
            //var index7 = GetColorOfPixel(X, Y + 1, _stride, pixels);
            //var index8 = GetColorOfPixel(X + 1, Y + 1, _stride, pixels);

            //var result = (index1 == 255 && index2 == 255 && index3 == 255 && index4 == 255 && index5 == 255
            //    && index6 == 255 && index6 == 255 && index7 == 255 && index8 == 255);
            //return result;

            return true;
        }

        public static bool CheckIfOnlyBlack(int X, int Y, int _stride, byte[] pixels)
        {
            for (int i = X - 1; i <= X + 1; i++)
            {
                if (GetColorOfPixel(i, Y - 1, _stride, pixels) == 255)
                {
                    return false;
                }
            }

            if (GetColorOfPixel(X - 1, Y, _stride, pixels) == 255 || GetColorOfPixel(X - 1, Y, _stride, pixels) == 255)
            {
                return false;
            }

            for (int i = X - 1; i <= X + 1; i++)
            {
                if (GetColorOfPixel(i, Y + 1, _stride, pixels) == 255)
                {
                    return false;
                }
            }

            //var result = (index1 == 0 && index2 == 0 && index3 == 0 && index4 == 0 && index5 == 0
            //    && index6 == 0 && index6 == 0 && index7 == 0 && index8 == 0);
            //return result;

            return true;
        }

        public static bool CheckIfOnlyBlack2(int X, int Y, int _stride, byte[] pixels)
        {

            for (int i = X - 2; i <= X + 2; i++)
            {
                if (GetColorOfPixel(i, Y - 2, _stride, pixels) == 255)
                {
                    return false;
                }
            }

            for (int i = X - 1; i <= X + 1; i++)
            {
                if (GetColorOfPixel(i, Y - 1, _stride, pixels) == 255)
                {
                    return false;
                }
            }

            for (int i = X - 1; i <= X + 1; i++)
            {
                if (GetColorOfPixel(i, Y + 1, _stride, pixels) == 255)
                {
                    return false;
                }
            }

            for (int i = X - 1; i <= X + 1; i++)
            {
                if (GetColorOfPixel(i, Y + 2, _stride, pixels) == 255)
                {
                    return false;
                }
            }

            if (GetColorOfPixel(X - 1, Y, _stride, pixels) == 255 || GetColorOfPixel(X + 1, Y, _stride, pixels) == 255
                || GetColorOfPixel(X - 2, Y, _stride, pixels) == 255 || GetColorOfPixel(X + 2, Y, _stride, pixels) == 255)
            {
                return false;
            }


            //var index1 = GetColorOfPixel(X - 1, Y - 1, _stride, pixels);
            //var index2 = GetColorOfPixel(X, Y - 1, _stride, pixels);
            //var index3 = GetColorOfPixel(X + 1, Y - 1, _stride, pixels);
            //var index9 = GetColorOfPixel(X - 2, Y - 1, _stride, pixels);
            //var index10 = GetColorOfPixel(X + 2 + 1, Y - 1, _stride, pixels);

            //var index11 = GetColorOfPixel(X - 1, Y - 2, _stride, pixels);
            //var index12 = GetColorOfPixel(X, Y - 2, _stride, pixels);
            //var index13 = GetColorOfPixel(X + 1, Y - 2, _stride, pixels);
            //var index14 = GetColorOfPixel(X - 2, Y - 2, _stride, pixels);
            //var index15 = GetColorOfPixel(X + 2 + 1, Y - 2, _stride, pixels);

            var index4 = GetColorOfPixel(X - 1, Y, _stride, pixels);
            var index5 = GetColorOfPixel(X + 1, Y, _stride, pixels);
            var index16 = GetColorOfPixel(X - 2, Y, _stride, pixels);
            var index17 = GetColorOfPixel(X + 2, Y, _stride, pixels);

            //var index6 = GetColorOfPixel(X - 1, Y + 1, _stride, pixels);
            //var index7 = GetColorOfPixel(X, Y + 1, _stride, pixels);
            //var index8 = GetColorOfPixel(X + 1, Y + 1, _stride, pixels);
            //var index18 = GetColorOfPixel(X - 2, Y + 1, _stride, pixels);
            //var index19 = GetColorOfPixel(X + 2, Y + 1, _stride, pixels);

            //var index20 = GetColorOfPixel(X - 1, Y + 2, _stride, pixels);
            //var index21 = GetColorOfPixel(X, Y + 2, _stride, pixels);
            //var index22 = GetColorOfPixel(X + 1, Y + 2, _stride, pixels);
            //var index23 = GetColorOfPixel(X - 2, Y + 2, _stride, pixels);
            //var index24 = GetColorOfPixel(X + 2, Y + 2, _stride, pixels);

            //var result = (index1 == 255 && index2 == 255 && index3 == 255 && index4 == 255 && index5 == 255
            //    && index6 == 255 && index6 == 255 && index7 == 255 && index8 == 255 && index9 == 255 && index10 == 255
            //    && index11 == 255 && index12 == 255 && index13 == 255 && index14 == 255 && index15 == 255 && index16 == 255
            //    && index17 == 255 && index18 == 255 && index19 == 255 && index20 == 255 && index21 == 255 && index22 == 255
            //    && index23 == 255 && index24 == 255);
            //return result;
            return true;
        }

        public static bool CheckIfOnlyWhite2(int X, int Y, int _stride, byte[] pixels)
        {

            for (int i = X - 2; i <= X + 2; i++)
            {
                if (GetColorOfPixel(i, Y - 2, _stride, pixels) == 0)
                {
                    return false;
                }
            }

            for (int i = X - 1; i <= X + 1; i++)
            {
                if (GetColorOfPixel(i, Y - 1, _stride, pixels) == 0)
                {
                    return false;
                }
            }

            for (int i = X - 1; i <= X + 1; i++)
            {
                if (GetColorOfPixel(i, Y + 1, _stride, pixels) == 0)
                {
                    return false;
                }
            }

            for (int i = X - 1; i <= X + 1; i++)
            {
                if (GetColorOfPixel(i, Y + 2, _stride, pixels) == 0)
                {
                    return false;
                }
            }

            if (GetColorOfPixel(X - 1, Y, _stride, pixels) == 0 || GetColorOfPixel(X + 1, Y, _stride, pixels) == 0
                || GetColorOfPixel(X -2, Y, _stride, pixels) == 0 || GetColorOfPixel(X + 2, Y, _stride, pixels) == 0)
            {
                return false;
            }


            //var index1 = GetColorOfPixel(X - 1, Y - 1, _stride, pixels);
            //var index2 = GetColorOfPixel(X, Y - 1, _stride, pixels);
            //var index3 = GetColorOfPixel(X + 1, Y - 1, _stride, pixels);
            //var index9 = GetColorOfPixel(X - 2, Y - 1, _stride, pixels);
            //var index10 = GetColorOfPixel(X + 2 + 1, Y - 1, _stride, pixels);

            //var index11 = GetColorOfPixel(X - 1, Y - 2, _stride, pixels);
            //var index12 = GetColorOfPixel(X, Y - 2, _stride, pixels);
            //var index13 = GetColorOfPixel(X + 1, Y - 2, _stride, pixels);
            //var index14 = GetColorOfPixel(X - 2, Y - 2, _stride, pixels);
            //var index15 = GetColorOfPixel(X + 2 + 1, Y - 2, _stride, pixels);

            var index4 = GetColorOfPixel(X - 1, Y, _stride, pixels);
            var index5 = GetColorOfPixel(X + 1, Y, _stride, pixels);
            var index16 = GetColorOfPixel(X - 2, Y, _stride, pixels);
            var index17 = GetColorOfPixel(X + 2, Y, _stride, pixels);

            //var index6 = GetColorOfPixel(X - 1, Y + 1, _stride, pixels);
            //var index7 = GetColorOfPixel(X, Y + 1, _stride, pixels);
            //var index8 = GetColorOfPixel(X + 1, Y + 1, _stride, pixels);
            //var index18 = GetColorOfPixel(X - 2, Y + 1, _stride, pixels);
            //var index19 = GetColorOfPixel(X + 2, Y + 1, _stride, pixels);

            //var index20 = GetColorOfPixel(X - 1, Y + 2, _stride, pixels);
            //var index21 = GetColorOfPixel(X, Y + 2, _stride, pixels);
            //var index22 = GetColorOfPixel(X + 1, Y + 2, _stride, pixels);
            //var index23 = GetColorOfPixel(X - 2, Y + 2, _stride, pixels);
            //var index24 = GetColorOfPixel(X + 2, Y + 2, _stride, pixels);

            //var result = (index1 == 255 && index2 == 255 && index3 == 255 && index4 == 255 && index5 == 255
            //    && index6 == 255 && index6 == 255 && index7 == 255 && index8 == 255 && index9 == 255 && index10 == 255
            //    && index11 == 255 && index12 == 255 && index13 == 255 && index14 == 255 && index15 == 255 && index16 == 255
            //    && index17 == 255 && index18 == 255 && index19 == 255 && index20 == 255 && index21 == 255 && index22 == 255
            //    && index23 == 255 && index24 == 255);
            //return result;
            return true;
        }


        public static int GetPixelValue(int x, int y, ImageProperties imgProp)
        {
            var index = GetIndexOfPixel(x, y, imgProp.Stride);
            return imgProp.ChangedPixels[index + 1];
        }


        public static int[] GetLUTForChangeTheBrightness(double x)
        {
            int[] LUT = new int[256];

            for (int i = 0; i < 256; i++)
            {
                //LUT[(int)i] = (int)(255 * Math.Pow(i / 255, x));
                LUT[i] = (int)(i + x);
                if (LUT[i] > 255)
                {
                    LUT[i] = 255;
                }
                if (LUT[i] < 0)
                {
                    LUT[i] = 0;
                }
            }
            return LUT;
        }

        public static int[] GetLUTForChangeTheBrightness2(int x)
        {
            int[] LUT = new int[256];
            var nmb = Math.Pow((x), 2);
            for (int i = 0; i < 256; i++)
            {
                LUT[i] = (int)(255 * Math.Log(1 + i) / Math.Log(1 + 255));
            }
            return LUT;
        }

        public static int[] GetLUTForHistogramStreching(byte[] pixels, double min, double max)
        {
            int[] LUT = new int[256];

            for (int i = 0; i <= 255; i++)
            {
                LUT[i] = (int)((255 / (max - min)) * (i - min));
                if (LUT[i] < 0) LUT[i] = 0;
                if (LUT[i] > 255) LUT[i] = 255;
            }
            return LUT;
        }

        public static int[] GetLUTFromCumulativeHistogram(int[] cumulativeHistogram, int width, int height)
        {
            var LUT = new int[256];

            for (int i = 0; i < 256; i++)
            {
                LUT[i] = cumulativeHistogram[i] * 255 / (width * height);
            }

            return LUT;
        }

        public static int[] GetCumulativeHistogram(int[] histogram)
        {
            var cumulativeHistogram = histogram;
            cumulativeHistogram[0] = histogram[0];
            for (int i = 1; i < 256; i++)
            {
                cumulativeHistogram[i] = cumulativeHistogram[i] + cumulativeHistogram[i - 1];
            }

            return cumulativeHistogram;
        }

        public void ChangePixel(int x, int y, int r, int g, int b)
        {
            var index = ImageOperations.GetIndexOfPixel(x, y, imgProperties.Stride);
            imgProperties.ChangedPixels[index + 2] = (byte)r;
            imgProperties.ChangedPixels[index + 1] = (byte)g;
            imgProperties.ChangedPixels[index] = (byte)b;
        }

        public bool[][] Image2Bool()
        {
            var s = new bool[imgProperties.Height][];
            for (var y = 0; y < imgProperties.Height; y++)
            {
                s[y] = new bool[imgProperties.Width];
                for (var x = 0; x < imgProperties.Width; x++)
                {
                    var index = ImageOperations.GetIndexOfPixel(x, y, imgProperties.Stride);
                    s[y][x] = imgProperties.Pixels[index] < 100;
                }
            }
            return s;
        }

        public byte[] Bool2Image(bool[][] s)
        {
            for (var y = 0; y < imgProperties.Height; y++)
            {
                for (var x = 0; x < imgProperties.Width; x++)
                {
                    if (s[y][x])
                    {
                        ChangePixel(x, y, 0, 0, 0);
                    }
                    else
                    {
                        ChangePixel(x, y, 255, 255, 255);
                    }
                }
            }

            return imgProperties.ChangedPixels;

        }

    }
}

