﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Biometria_Projekt.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy Bezir.xaml
    /// </summary>
    public partial class Bezir : Window
    {
        BezierSegment bezier;
        public Bezir()
        {
            InitializeComponent();

        }

        Ellipse CreateEllipse(double width, double height, double desiredCenterX, double desiredCenterY)
        {
            Ellipse ellipse = new Ellipse { Width = width, Height = height };
            ellipse.Fill = Brushes.Blue;
            return ellipse;
        }

        private void DrawBezir(object sender, RoutedEventArgs e)
        {
            BezirCanvas.Children.Clear();
            
            var p1x = Int32.Parse(P1X.Text);
            var p1y = Int32.Parse(P1Y.Text);
            var p2x = Int32.Parse(P2X.Text);
            var p2y = Int32.Parse(P2Y.Text);
            var p3x = Int32.Parse(P3X.Text);
            var p3y = Int32.Parse(P3Y.Text);

            bezier = new BezierSegment()
            {
                Point1 = new Point(p1x, p1y),
                Point2 = new Point(p2x, p2y),
                Point3 = new Point(p3x, p3y),
                IsStroked = true
            };

            var ellipse1 = CreateEllipse(10, 10, p1x, p1y);
            var ellipse2 = CreateEllipse(10, 10, p2x, p2y);
            var ellipse3 = CreateEllipse(10, 10, p3x, p3y);

            PathFigure figure = new PathFigure();
            figure.Segments.Add(bezier);

            Path path = new Path();
            path.Stroke = Brushes.Black;
            path.Data = new PathGeometry(new PathFigure[] { figure });

            Canvas.SetLeft(ellipse1, p1x);
            Canvas.SetTop(ellipse1, p1y);
            Canvas.SetLeft(ellipse2, p2x);
            Canvas.SetTop(ellipse2, p2y);
            Canvas.SetLeft(ellipse3, p3x);
            Canvas.SetTop(ellipse3, p3y);

            BezirCanvas.Children.Add(path);
            BezirCanvas.Children.Add(ellipse1);
            BezirCanvas.Children.Add(ellipse2);
            BezirCanvas.Children.Add(ellipse3);




        }

        private Point GetPoint(double t, Point p0, Point p1, Point p2, Point p3)
        {
            double cx = 3 * (p1.X - p0.X);
            double cy = 3 * (p1.Y - p0.Y);
            double bx = 3 * (p2.X - p1.X) - cx;
            double by = 3 * (p2.Y - p1.Y) - cy;
            double ax = p3.X - p0.X - cx - bx;
            double ay = p3.Y - p0.Y - cy - by;
            double Cube = t * t * t;
            double Square = t * t;

            double resX = (ax * Cube) + (bx * Square) + (cx * t) + p0.X;
            double resY = (ay * Cube) + (by * Square) + (cy * t) + p0.Y;

            return new Point(resX, resY);
        }

    }
}
