﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Biometria_Projekt.Windows
{
    /// <summary>
    /// Interaction logic for Cube3D.xaml
    /// </summary>
    public partial class Cube3D : Window
    {
        public Cube3D()
        {
            InitializeComponent();
  
        }

        public Cube3D(byte red, byte green, byte blue)
        {
            InitializeComponent();
            matDiffuseMain.Brush = new SolidColorBrush(Color.FromRgb(red, green, blue));
        }


    }
}
