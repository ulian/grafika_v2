﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Biometria_Projekt.Windows
{
    /// <summary>
    /// Interaction logic for converter.xaml
    /// </summary>
    public partial class converter : Window
    {
        public converter()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var helpMin = new List<Double>();
                var red = Convert.ToDouble(RedTxt.Text) / 255.0;
                var green = Convert.ToDouble(GreenTxt.Text) / 255.0;
                var blue = Convert.ToDouble(BlueTxt.Text) / 255.0;
                helpMin.Add(red);
                helpMin.Add(green);
                helpMin.Add(blue);

                var black = helpMin.Min(x => x);
                var cyan = (1 - red - black) / (1 - black);
                var magatena = (1 - green - black) / (1 - black);
                var yellow = (1 - blue - black) / (1 - black);

                BlackTxt.Text = black.ToString();
                CyanTxt.Text = cyan.ToString();
                MagentaTxt.Text = magatena.ToString();
                YellowTxt.Text = yellow.ToString();

                RgbColor.Fill = new SolidColorBrush(Color.FromRgb(Convert.ToByte(red * 255), Convert.ToByte(green * 255), Convert.ToByte(blue * 255)));
            }
            catch
            {
                MessageBox.Show("Podano złe wartości");
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                var helpMin = new List<Double>();

                var black = Convert.ToDouble(BlackTxt.Text);
                var cyan = Convert.ToDouble(CyanTxt.Text);
                var magenta = Convert.ToDouble(MagentaTxt.Text);
                var yellow = Convert.ToDouble(YellowTxt.Text);

                byte red = Convert.ToByte((1 - Math.Min(1, cyan * (1 - black) + black)) * 255);
                byte green = Convert.ToByte((1 - Math.Min(1, magenta * (1 - black) + black)) * 255);
                byte blue = Convert.ToByte((1 - Math.Min(1, yellow * (1 - black) + black)) * 255);

                RedTxt.Text = red.ToString();
                GreenTxt.Text = green.ToString();
                BlueTxt.Text = blue.ToString();

                RgbColor.Fill = new SolidColorBrush(Color.FromRgb(red, green, blue));
            }
            catch
            {
                MessageBox.Show("Podano złe wartości");
            }
        }

        private void RgbCubeClick(object sender, RoutedEventArgs e)
        {
            var red = Convert.ToByte(RedTxt.Text);
            var green = Convert.ToByte(GreenTxt.Text);
            var blue = Convert.ToByte(BlueTxt.Text);
            var cube = new Cube3D(red, green, blue);
            cube.Show();
        }
    }
}
